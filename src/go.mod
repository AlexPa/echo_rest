module rest_api_v2

go 1.20

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-faker/faker/v4 v4.1.0
	github.com/go-sql-driver/mysql v1.7.0
	github.com/golang-jwt/jwt/v4 v4.4.3
	github.com/gookit/validate v1.4.6
	github.com/gosimple/slug v1.13.1
	github.com/ilyakaznacheev/cleanenv v1.4.2
	github.com/jmoiron/sqlx v1.3.5
	github.com/labstack/echo-jwt/v4 v4.1.0
	github.com/labstack/echo/v4 v4.10.2
	github.com/labstack/gommon v0.4.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.1
	go.mongodb.org/mongo-driver v1.11.4
	golang.org/x/crypto v0.6.0
	golang.org/x/time v0.3.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gookit/filter v1.1.4 // indirect
	github.com/gookit/goutil v0.5.15 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
