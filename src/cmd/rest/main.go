package main

import (
	_ "github.com/go-sql-driver/mysql"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"net/http"
	"rest_api_v2/internal/config"
	"rest_api_v2/internal/handler"
	"rest_api_v2/internal/logger"
	"rest_api_v2/internal/storage_handler"
)

func main() {
	e := echo.New()

	standardLogger := e.Logger

	logRus := logger.GetLogger()
	logRus.SetLevel(logrus.DebugLevel)
	logRus.SetFormatter(&logrus.TextFormatter{})

	e.Logger = logger.MyLogger{}.NewLogger(logRus, standardLogger)

	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:    true,
		LogStatus: true,
		LogValuesFunc: func(c echo.Context, values middleware.RequestLoggerValues) error {
			logRus.WithFields(logrus.Fields{
				"URI":    values.URI,
				"status": values.Status,
			}).Info("request")

			return nil
		},
	}))
	e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(config.GetConfig().RateLimit)))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     config.GetConfig().FriendlyUrls,
		AllowCredentials: true,
	}))

	storageHandler := &storage_handler.StorageHandler{
		Driver: config.GetConfig().DatabaseDriver,
		E:      e,
	}
	e.Logger.Info(storageHandler)
	//defer storageHandler.DB.Close()

	userStorage := storageHandler.GetUserStorage()
	postStorage := storageHandler.GetPostStorage()

	userContr := handler.UserController{UserStorage: userStorage}
	postContr := handler.PostController{
		PostStorage: postStorage,
		UserStorage: userStorage,
	}

	e.POST("/user/register", userContr.Register)
	e.POST("/user/login", userContr.Login)

	jwtMiddleware := echojwt.JWT([]byte(config.GetConfig().JwtSecretKey))

	e.GET("/api/posts", postContr.List, jwtMiddleware)
	e.POST("/api/posts", postContr.Create, jwtMiddleware)
	e.GET("/api/posts/:id", postContr.GetOne, jwtMiddleware)
	e.DELETE("/api/posts/:id", postContr.Delete, jwtMiddleware)
	e.PUT("/api/posts/:id", postContr.Update, jwtMiddleware)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":1323"))
}
