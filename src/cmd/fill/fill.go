package main

import (
	"github.com/go-faker/faker/v4"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gosimple/slug"
	"github.com/labstack/echo/v4"
	"rest_api_v2/internal/config"
	post2 "rest_api_v2/internal/post"
	"rest_api_v2/internal/storage_handler"
	user2 "rest_api_v2/internal/user"
	"time"
)

func main() {
	e := echo.New()
	storageHandler := &storage_handler.StorageHandler{
		Driver: config.GetConfig().DatabaseDriver,
		E:      e,
	}

	userStorage := storageHandler.GetUserStorage()
	postStorage := storageHandler.GetPostStorage()

	for i := 0; i < 100; i++ {
		user := user2.User{
			Name:      faker.FirstName(),
			Email:     faker.Email(),
			CreatedAt: time.Now(),
		}
		user.PasswordHash = user.HashPassword("12345")
		e.Logger.Info("User: ", user)
		userStorage.Create(user)
	}
	for i := 0; i < 100; i++ {
		usr, err := userStorage.GetRandom()
		if err != nil {
			continue
		}
		ttl := faker.Sentence()
		post := post2.Post{
			UserId:    usr.Id,
			Title:     ttl,
			Body:      faker.Paragraph(),
			Slug:      slug.Make(ttl),
			File:      "",
			CreatedAt: time.Now(),
		}
		postStorage.Create(post)
	}

}
