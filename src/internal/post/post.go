package post

import (
	"time"
)

type Post struct {
	Id        string    `json:"id" db:"id" bson:"_id,omitempty"`
	UserId    string    `json:"user_id" db:"user_id" bson:"user_id,omitempty" validate:"required"`
	Title     string    `json:"title" db:"title" bson:"title,omitempty" validate:"required"`
	Body      string    `json:"body" db:"body" bson:"body,omitempty" validate:"required"`
	Slug      string    `json:"slug" db:"slug" bson:"slug,omitempty" validate:"required"`
	File      string    `json:"file" db:"file" bson:"file"`
	CreatedAt time.Time `json:"created_at" db:"created_at" bson:"created_at,omitempty"`
}
