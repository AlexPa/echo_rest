package mongo

import (
	"context"
	"errors"
	"fmt"
	"github.com/gosimple/slug"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"rest_api_v2/internal/post"
	"time"
)

type Db struct {
	collection *mongo.Collection
	logger     echo.Logger
	ctx        context.Context
}

func (d *Db) GetList(filter post.Filter) (models *[]post.Post, err error) {
	opt := options.Find()
	// Sort by `_id` field descending
	opt.SetSort(bson.D{{"created_at", -1}})
	// Limit by 10 documents only
	opt.SetLimit(int64(filter.Limit))
	opt.SetSkip(int64(filter.Start))

	var f = bson.M{}
	cursor, err := d.collection.Find(d.ctx, f, opt)

	if err != nil {
		return nil, err
	}

	if cursor.Err() != nil {
		return nil, err
	}

	var ms []post.Post
	err = cursor.All(d.ctx, &ms)
	if err != nil {
		return nil, err
	}

	return &ms, nil
}

func (d *Db) Update(post post.Post) (model *post.Post, err error) {
	post.CreatedAt = time.Now()
	post.Slug = slug.Make(post.Title)
	oid, err := primitive.ObjectIDFromHex(post.Id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", post.Id)
		return model, err
	}
	var filter = bson.M{"_id": oid}
	userBytes, err := bson.Marshal(post)
	if err != nil {
		d.logger.Errorf("Can not marshal model structure to bytes %v", err)
		return model, err
	}
	var updateUserObj bson.M
	err = bson.Unmarshal(userBytes, &updateUserObj)
	if err != nil {
		d.logger.Errorf("Can not convert bytes to updateBson %v", err)
		return model, err
	}
	delete(updateUserObj, "_id")
	updateBson := bson.M{"$set": updateUserObj}

	result, err := d.collection.UpdateOne(d.ctx, filter, updateBson)
	if err != nil {
		d.logger.Errorf("Can not update %v", err)
		return model, err
	}
	if result.MatchedCount == 0 {
		return model, errors.New("not found")
	}
	usr, _ := d.GetById(post.Id)
	return usr, nil
}

func (d *Db) Delete(id string) error {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", id)
		return err
	}
	var filter = bson.M{"_id": oid}

	deleteResult, err := d.collection.DeleteOne(d.ctx, filter)
	if err != nil {
		d.logger.Errorf("Can not delete model %v", err)
		return err
	}
	if deleteResult.DeletedCount == 0 {
		return nil
	}
	return nil
}

func (d *Db) GetById(id string) (model *post.Post, err error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", id)
		return model, err
	}
	var filter = bson.M{"_id": oid}
	result := d.collection.FindOne(d.ctx, filter)
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return model, errors.New("not found")
	}
	if result.Err() != nil {
		return model, err
	}
	err = result.Decode(&model)
	if err != nil {
		return model, err
	}
	return model, nil
}

func (d *Db) Create(post post.Post) (*post.Post, error) {
	post.CreatedAt = time.Now()
	post.Slug = slug.Make(post.Title)
	d.logger.Debug(post)
	res, err := d.collection.InsertOne(d.ctx, post)
	if err != nil {
		d.logger.Error(err)
		return nil, fmt.Errorf("Can not create model because %s ", err)
	}
	d.logger.Debug("Res is: ", res)
	id := res.InsertedID.(primitive.ObjectID)
	d.logger.Debugf("Model was created with id %s", id.Hex())
	usr, _ := d.GetById(id.Hex())
	return usr, nil
}

func NewStorage(collection *mongo.Collection, l echo.Logger, ctx context.Context) post.Storage {
	return &Db{
		collection: collection,
		logger:     l,
		ctx:        ctx,
	}
}
