package mysql

import (
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"rest_api_v2/internal/post"
	"strconv"
	"time"
)

type Db struct {
	sql    *sqlx.DB
	Logger echo.Logger
}

func (d *Db) GetList(filter post.Filter) (*[]post.Post, error) {
	var models []post.Post
	err := d.sql.Select(&models, "SELECT * FROM `posts` ORDER by id DESC LIMIT ? OFFSET ?", filter.Limit, filter.Start)
	if errors.Is(err, sql.ErrNoRows) {
		d.Logger.Debug("Models not found %s", err)
		return nil, nil
	}
	d.Logger.Infof("Models: %s", models)
	return &models, err
}

func (d *Db) Update(post post.Post) (*post.Post, error) {
	post.CreatedAt = time.Now()
	res, err := d.sql.NamedExec("UPDATE `posts` SET  `title`=:title, `body`=:body, `slug`=:slug WHERE id=:id", post)
	d.Logger.Debug("Update post error: ", err)
	d.Logger.Debug("Update post result: ", res)
	return d.GetById(post.Id)
}

func (d *Db) Delete(id string) error {
	res, err := d.sql.Exec("DELETE FROM `posts` WHERE id=?", id)
	d.Logger.Debug("Delete model error: ", err)
	d.Logger.Debug("Delete model result: ", res)
	if err != nil {
		return err
	}
	return nil
}

func (d *Db) Create(post post.Post) (*post.Post, error) {
	post.CreatedAt = time.Now()
	res, err := d.sql.NamedExec("INSERT INTO `posts` (`title`, `user_id`, `body`, `slug`, `created_at`, `file`) VALUES (:title,:user_id,:body,:slug,:created_at,:file)", post)
	d.Logger.Debug("Create post error: ", err)
	d.Logger.Debug("Create post result: ", res)
	id, _ := res.LastInsertId()
	d.Logger.Infof("NEW ID: %d", id)
	strId := strconv.FormatInt(id, 10)
	d.Logger.Infof("NEW ID (str): %s", strId)
	return d.GetById(strId)
}

func (d *Db) GetById(id string) (*post.Post, error) {
	var model post.Post
	err := d.sql.Get(&model, "SELECT * FROM `posts` WHERE `id`=? LIMIT 1", id)
	if errors.Is(err, sql.ErrNoRows) {
		d.Logger.Infof("Model not found by id=%s: %s", id, err)
		return nil, nil
	}
	if err != nil {
		d.Logger.Error("GetById err: ", err)
		return nil, err
	}
	d.Logger.Infof("Model found by id=%s: %s", id, &model)
	return &model, err
}

func NewStorage(sql *sqlx.DB, l echo.Logger) post.Storage {
	return &Db{sql: sql, Logger: l}
}
