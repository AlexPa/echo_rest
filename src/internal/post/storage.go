package post

type Filter struct {
	Start int
	Limit int
}

type Storage interface {
	GetById(id string) (*Post, error)
	Create(post Post) (*Post, error)
	GetList(filter Filter) (*[]Post, error)
	Update(post Post) (*Post, error)
	Delete(id string) error
}
