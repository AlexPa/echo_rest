package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/time/rate"
	"sync"
)

type Config struct {
	IsDebug           *bool      `yaml:"is_debug" env:"IS_DEBUG"`
	RateLimit         rate.Limit `yaml:"rate_limit" env:"RATE_LIMIT"`
	BaseUrl           string     `yaml:"base_url" env:"BASE_URL"`
	FriendlyUrls      []string   `yaml:"friendly_urls" env:"FRIENDLY_URLS"`
	DatabaseDriver    string     `yaml:"database_driver" env:"DATABASE_DRIVER"`
	JwtSecretKey      string     `yaml:"jwt_secret_key" env:"JWT_SECRET_KEY"`
	JwtTokenAliveDays int        `yaml:"jwt_token_alive_days" env:"JWT_TOKEN_ALIVE_DAYS"`

	MysqlHost     string `yaml:"mysql_host" env:"MYSQL_HOST"`
	MysqlPort     string `yaml:"mysql_port" env:"MYSQL_PORT"`
	MysqlDatabase string `yaml:"mysql_database" env:"MYSQL_DATABASE"`
	MysqlUsername string `yaml:"mysql_username" env:"MYSQL_USERNAME"`
	MysqlPassword string `yaml:"mysql_password" env:"MYSQL_PASSWORD"`

	MongodbHost     string `yaml:"mongodb_host" env:"MONGODB_HOST"`
	MongodbPort     string `yaml:"mongodb_port" env:"MONGODB_PORT"`
	MongodbDatabase string `yaml:"mongodb_database" env:"MONGODB_DATABASE"`
	MongodbUsername string `yaml:"mongodb_username" env:"MONGODB_USERNAME"`
	MongodbPassword string `yaml:"mongodb_password" env:"MONGODB_PASSWORD"`
	MongodbAuthDb   string `yaml:"mongodb_auth_db" env:"MONGODB_AUTH_DB"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {
		middleware.Logger()
		instance = &Config{}
		//err := cleanenv.ReadEnv(instance)
		err := cleanenv.ReadConfig("config.yml", instance)
		if err != nil {
			desc, _ := cleanenv.GetDescription(instance, nil)
			panic(desc)
		}
	})
	return instance
}
