package storage_handler

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	mongo3 "go.mongodb.org/mongo-driver/mongo"
	"rest_api_v2/internal/client/mongodb"
	"rest_api_v2/internal/config"
	"rest_api_v2/internal/post"
	mongo2 "rest_api_v2/internal/post/impl/mongo"
	mysql2 "rest_api_v2/internal/post/impl/mysql"
	"rest_api_v2/internal/user"
	"rest_api_v2/internal/user/impl/mongo"
	"rest_api_v2/internal/user/impl/mysql"
)

const DriverMysql string = "mysql"
const DriverMongo string = "mongo"

type StorageHandler struct {
	Driver string
	E      *echo.Echo
	DB     *sqlx.DB
}

func (sh *StorageHandler) GetMysqlConnection() *sqlx.DB {
	sh.E.Logger.Debug("This may be called many times")
	if sh.DB != nil {
		sh.E.Logger.Debug("This must be called one times")
		return sh.DB
	}
	cfg := config.GetConfig()
	conStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
		cfg.MysqlUsername,
		cfg.MysqlPassword,
		cfg.MysqlHost,
		cfg.MysqlPort,
		cfg.MysqlDatabase,
	)

	sh.E.Logger.Debug("SQL connect str: ", conStr)
	var err error
	if sh.DB, err = sqlx.Connect("mysql", conStr); err != nil {
		sh.E.Logger.Error(err)
		panic("Can't open mysql connection.")
	}
	return sh.DB
}
func (sh *StorageHandler) GetMongoCollection(collection string) *mongo3.Collection {
	cfg := config.GetConfig()
	storageClient := mongodb.NewClient(context.Background(), mongodb.MongoClientConfig{
		Host:     cfg.MongodbHost,
		Port:     cfg.MongodbPort,
		Username: cfg.MongodbUsername,
		Password: cfg.MongodbPassword,
		Database: cfg.MongodbDatabase,
		AuthDB:   cfg.MongodbAuthDb,
	})
	return storageClient.Collection(collection)
}

func (sh *StorageHandler) GetUserStorage() user.Storage {
	if sh.Driver == DriverMysql {
		return mysql.NewStorage(sh.GetMysqlConnection(), sh.E.Logger)
	}
	if sh.Driver == DriverMongo {
		return mongo.NewStorage(sh.GetMongoCollection("users"), sh.E.Logger, context.Background())
	}
	panic("Impossible database driver")
}

func (sh *StorageHandler) GetPostStorage() post.Storage {
	if sh.Driver == DriverMysql {
		return mysql2.NewStorage(sh.GetMysqlConnection(), sh.E.Logger)
	}
	if sh.Driver == DriverMongo {
		return mongo2.NewStorage(sh.GetMongoCollection("posts"), sh.E.Logger, context.Background())
	}
	panic("Impossible database driver")
}
