package handler

import (
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gookit/validate"
	"github.com/labstack/echo/v4"
	"net/http"
	"rest_api_v2/internal/config"
	"rest_api_v2/internal/user"
	"time"
)

type jwtCustomClaims struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	IsAdmin bool   `json:"is_admin"`
	jwt.RegisteredClaims
}

type UserController struct {
	UserStorage user.Storage
}

func (contr UserController) Login(c echo.Context) error {
	email := c.FormValue("email")
	password := c.FormValue("password")

	usr, err := contr.UserStorage.GetByEmail(email)
	c.Logger().Debug("User by email is: ", usr)
	// Throws unauthorized error
	if usr == nil || err != nil || (usr != nil && !usr.IsPasswordValid(password)) {
		return echo.ErrUnauthorized
	}

	// Set custom claims
	claims := &jwtCustomClaims{
		usr.Id,
		usr.Name,
		false,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24 * time.Duration(config.GetConfig().JwtTokenAliveDays))),
		},
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(config.GetConfig().JwtSecretKey))
	if err != nil {
		return err
	}

	res, err := contr.UserStorage.UpdateLoggedAtField(usr)
	c.Logger().Debugf("Update logged_at field, res: %s, err: %s", res, err)

	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}
func (contr UserController) Register(c echo.Context) error {
	email := c.FormValue("email")
	password := c.FormValue("password")

	cu := user.CreateUser{
		Name:        email,
		Email:       email,
		Password:    password,
		UserStorage: contr.UserStorage,
	}
	v := validate.Struct(cu)
	if !v.Validate() {
		errors := v.Errors
		c.Logger().Error(fmt.Sprintf("Can't validate. Error: %s", errors))
		return c.JSON(http.StatusBadRequest, echo.Map{
			"errors": errors,
		})
	}
	u := user.User{}.GetUserForCreate(cu)
	resUser, err := contr.UserStorage.Create(u)
	if err != nil {
		c.Logger().Error(fmt.Sprintf("Can't register user. Error: %f", err))
		return err
	}
	return c.JSON(http.StatusCreated, echo.Map{
		"user": resUser,
	})

}
