package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"rest_api_v2/internal/post"
	"rest_api_v2/internal/user"
	"testing"
	"time"
)

var p1 = post.Post{
	Id:        "1",
	UserId:    "1",
	Title:     "Title",
	Body:      "Body",
	Slug:      "Slug",
	File:      "",
	CreatedAt: time.Date(2023, 4, 8, 0, 0, 0, 0, time.Local),
}
var p2 = post.Post{
	Id:        "1",
	UserId:    "1",
	Title:     "Title",
	Body:      "Body",
	Slug:      "Slug",
	File:      "",
	CreatedAt: time.Date(2023, 4, 8, 6, 0, 0, 0, time.Local),
}

type PostMockStorage struct {
}

func (p PostMockStorage) GetById(id string) (*post.Post, error) {
	//TODO implement me
	panic("implement me")
}

func (p PostMockStorage) Create(post post.Post) (*post.Post, error) {
	//TODO implement me
	panic("implement me")
}

func (p PostMockStorage) GetList(filter post.Filter) (*[]post.Post, error) {
	var posts []post.Post
	posts = append(posts, p1)
	posts = append(posts, p2)
	return &posts, nil
}

func (p PostMockStorage) Update(post post.Post) (*post.Post, error) {
	//TODO implement me
	panic("implement me")
}

func (p PostMockStorage) Delete(id string) error {
	//TODO implement me
	panic("implement me")
}

type UserMockStorage struct {
}

func (u UserMockStorage) UpdateLoggedAtField(user *user.User) (bool, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserMockStorage) Create(user user.User) (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserMockStorage) GetByEmail(email string) (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserMockStorage) GetById(id string) (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserMockStorage) GetRandom() (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func TestList(t *testing.T) {
	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, "/api/posts", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	postMockStorage := PostMockStorage{}
	userMockStorage := UserMockStorage{}

	controller := PostController{
		PostStorage: postMockStorage,
		UserStorage: userMockStorage,
	}

	if assert.NoError(t, controller.List(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
