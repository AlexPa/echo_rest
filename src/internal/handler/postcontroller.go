package handler

import (
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gookit/validate"
	"github.com/gosimple/slug"
	"github.com/labstack/echo/v4"
	"net/http"
	"rest_api_v2/internal/post"
	"rest_api_v2/internal/user"
	"strconv"
)

type PostController struct {
	PostStorage post.Storage
	UserStorage user.Storage
}

func (contr PostController) List(c echo.Context) error {
	page := 1
	startString := c.QueryParam("page")
	page, err := strconv.Atoi(startString)
	if err != nil {
		page = 1
	}
	pageSize := 10
	filter := post.Filter{
		Start: (page - 1) * pageSize,
		Limit: pageSize,
	}
	c.Logger().Debug("Filter: ", filter)
	models, err := contr.PostStorage.GetList(filter)
	if err != nil {
		c.Logger().Error("Can't find models: ", err)
		return err
	}
	return c.JSON(http.StatusOK, echo.Map{
		"data": models,
	})
}
func (contr PostController) getUserFromContext(c echo.Context) *user.User {
	userFromToken := c.Get("user").(*jwt.Token)
	claims := userFromToken.Claims.(jwt.MapClaims)

	usr, err := contr.UserStorage.GetById(claims["id"].(string))
	if err != nil {
		c.Logger().Error("Can't find usr by jwt token: ", err)
		return nil
	}
	return usr
}
func (contr PostController) Create(c echo.Context) error {
	usr := contr.getUserFromContext(c)
	if usr == nil {
		return echo.ErrUnauthorized
	}

	var p post.Post
	err := c.Bind(&p)
	if err != nil {
		c.Logger().Error("Can't bind data: ", err)
	}
	p.UserId = usr.Id
	p.Slug = slug.Make(p.Title)

	v := validate.Struct(p)
	if !v.Validate() {
		errors := v.Errors
		c.Logger().Error(fmt.Sprintf("Can't validate. Error: %s", errors))
		return c.JSON(http.StatusBadRequest, echo.Map{
			"errors": errors,
		})
	}

	_, err = contr.PostStorage.Create(p)
	if err != nil {
		c.Logger().Error("Can't save post: ", err)
		return err
	}
	return c.String(http.StatusCreated, "")
}

func (contr PostController) GetOne(c echo.Context) error {
	id := c.Param("id")
	p, err := contr.PostStorage.GetById(id)
	if err != nil {
		c.Logger().Error("Can't find post: ", err)
		return echo.ErrNotFound
	}
	if p == nil {
		return echo.ErrNotFound
	}
	return c.JSON(http.StatusOK, echo.Map{
		"data": p,
	})
}

func (contr PostController) Delete(c echo.Context) error {
	id := c.Param("id")
	if err := contr.PostStorage.Delete(id); err != nil {
		c.Logger().Error("Can't delete post: ", err)
		return err
	}
	return c.String(http.StatusNoContent, "")
}

func (contr PostController) Update(c echo.Context) error {
	id := c.Param("id")
	p, err := contr.PostStorage.GetById(id)
	if err != nil {
		c.Logger().Error("Can't find post: ", err)
		return echo.ErrNotFound
	}
	if p == nil {
		return echo.ErrNotFound
	}

	err = c.Bind(&p)
	if err != nil {
		c.Logger().Error("Can't bind data: ", err)
	}
	p.Slug = slug.Make(p.Title)
	c.Logger().Debug(p)
	if v := validate.Struct(p); !v.Validate() {
		errors := v.Errors
		c.Logger().Error(fmt.Sprintf("Can't validate. Error: %s", errors))
		return c.JSON(http.StatusBadRequest, echo.Map{
			"errors": errors,
		})
	}

	if _, err = contr.PostStorage.Update(*p); err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"errors": err,
		})
	}
	return c.String(http.StatusNoContent, "")
}
