package user

type Storage interface {
	Create(user User) (*User, error)
	GetByEmail(email string) (*User, error)
	GetById(id string) (*User, error)
	GetRandom() (*User, error)
	UpdateLoggedAtField(user *User) (bool, error)
}
