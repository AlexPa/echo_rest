package user

import (
	"crypto/rand"
	"encoding/base64"
	"golang.org/x/crypto/bcrypt"
	"math"
	"time"
)

const mongoCollection string = "user"

type User struct {
	Id           string    `db:"id" bson:"_id,omitempty"`
	Name         string    `db:"name" bson:"name,omitempty"`
	Email        string    `db:"email" bson:"email,omitempty"`
	PasswordHash string    `db:"password_hash" bson:"password_hash,omitempty"`
	CreatedAt    time.Time `db:"created_at" bson:"created_at,omitempty"`
	LoggedAt     time.Time `db:"logged_at" bson:"logged_at"`
}

type CreateUser struct {
	Name        string `validate:"required" bson:"name,omitempty"`
	Email       string `validate:"required:required|email|ExistsValidator" bson:"email,omitempty"`
	Password    string `validate:"required|min_len:5" bson:"password,omitempty"`
	UserStorage Storage
}

func (u CreateUser) ExistsValidator(val string) bool {
	user, _ := u.UserStorage.GetByEmail(val)
	if user != nil {
		return false
	}
	return true
}

func (u User) GetUserForCreate(cu CreateUser) User {
	return User{
		Name:         cu.Name,
		Email:        cu.Email,
		PasswordHash: u.HashPassword(cu.Password),
		CreatedAt:    time.Now(),
	}
}
func (u User) randomBase64String(l int) string {
	buff := make([]byte, int(math.Ceil(float64(l)/float64(1.33333333333))))
	rand.Read(buff)
	str := base64.RawURLEncoding.EncodeToString(buff)
	return str[:l] // strip 1 extra character we get from odd length results
}

func (u User) HashPassword(password string) string {
	pass := []byte(password)
	// TODO Handle error
	hashedPassword, _ := bcrypt.GenerateFromPassword(pass, bcrypt.DefaultCost)

	return string(hashedPassword)
}
func (u User) IsPasswordValid(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(password))
	if err != nil {
		return false
	}
	return true
}
