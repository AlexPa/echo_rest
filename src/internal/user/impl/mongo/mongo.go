package mongo

import (
	"context"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"math/rand"
	"rest_api_v2/internal/user"
	"time"
)

type Db struct {
	collection *mongo.Collection
	logger     echo.Logger
	ctx        context.Context
}

func (d Db) UpdateLoggedAtField(user *user.User) (bool, error) {
	user.LoggedAt = time.Now()
	oid, err := primitive.ObjectIDFromHex(user.Id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", user.Id)
		return false, err
	}
	var filter = bson.M{"_id": oid}
	userBytes, err := bson.Marshal(user)
	if err != nil {
		d.logger.Errorf("Can not marshal model structure to bytes %v", err)
		return false, err
	}
	var updateUserObj bson.M
	err = bson.Unmarshal(userBytes, &updateUserObj)
	if err != nil {
		d.logger.Errorf("Can not convert bytes to updateBson %v", err)
		return false, err
	}
	delete(updateUserObj, "_id")
	updateBson := bson.M{"$set": updateUserObj}

	result, err := d.collection.UpdateOne(d.ctx, filter, updateBson)
	if err != nil {
		d.logger.Errorf("Can not update %v", err)
		return false, err
	}
	if result.MatchedCount == 0 {
		return false, errors.New("not found")
	}
	return true, nil
}

func (d Db) Create(user user.User) (*user.User, error) {
	d.logger.Debug(user)
	res, err := d.collection.InsertOne(d.ctx, user)
	if err != nil {
		d.logger.Error(err)
		return nil, fmt.Errorf("Can not create model because %s ", err)
	}
	d.logger.Debug("Res is: ", res)
	id := res.InsertedID.(primitive.ObjectID)
	d.logger.Debugf("Model was created with id %s", id.Hex())
	usr, _ := d.GetById(id.Hex())
	return usr, nil
}

func (d Db) GetByEmail(email string) (user *user.User, err error) {
	var filter = bson.M{"email": email}
	result := d.collection.FindOne(d.ctx, filter)
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return user, errors.New("not found")
	}
	if result.Err() != nil {
		return user, err
	}
	err = result.Decode(&user)
	if err != nil {
		return user, err
	}
	return user, nil
}

func (d Db) GetById(id string) (user *user.User, err error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", id)
		return user, err
	}
	var filter = bson.M{"_id": oid}
	result := d.collection.FindOne(d.ctx, filter)
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return user, errors.New("not found")
	}
	if result.Err() != nil {
		return user, err
	}
	err = result.Decode(&user)
	if err != nil {
		return user, err
	}
	return user, nil
}

func (d Db) GetRandom() (user *user.User, err error) {
	var f = bson.M{}
	cnt, err := d.collection.CountDocuments(d.ctx, f)
	if err != nil {
		return nil, err
	}
	opt := options.FindOne()
	rand := rand.Intn(int(cnt-0)) + 0
	opt.SetSkip(int64(rand))

	result := d.collection.FindOne(d.ctx, f, opt)

	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return user, errors.New("not found")
	}
	if result.Err() != nil {
		return user, err
	}
	err = result.Decode(&user)
	if err != nil {
		return user, err
	}
	return user, nil
}

func NewStorage(collection *mongo.Collection, l echo.Logger, ctx context.Context) user.Storage {
	return &Db{
		collection: collection,
		logger:     l,
		ctx:        ctx,
	}
}
