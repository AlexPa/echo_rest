package mysql

import (
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"rest_api_v2/internal/user"
	"strconv"
	"time"
)

type Db struct {
	sql    *sqlx.DB
	Logger echo.Logger
}

func (d *Db) UpdateLoggedAtField(user *user.User) (bool, error) {
	user.LoggedAt = time.Now()
	if res, err := d.sql.NamedExec("UPDATE `users` SET  `logged_at`=:logged_at WHERE id=:id", user); err != nil {
		d.Logger.Debug("Update user error: ", err)
		return false, err
	} else {
		d.Logger.Debug("Update user result: ", res)
	}
	return true, nil
}

func (d *Db) GetRandom() (*user.User, error) {
	var usr user.User
	err := d.sql.Get(&usr, "SELECT * FROM `users` ORDER BY RAND()LIMIT 1")
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	if err != nil {
		d.Logger.Error("GetRandom err: ", err)
		return nil, err
	}
	return &usr, err
}

func (d *Db) GetByEmail(email string) (u *user.User, e error) {
	var usr user.User
	err := d.sql.Get(&usr, "SELECT * FROM `users` WHERE `email`=? LIMIT 1", email)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	if err != nil {
		d.Logger.Error("GetByEmail err: ", err)
		return nil, err
	}
	return &usr, err
}
func (d *Db) GetById(id string) (u *user.User, e error) {
	var usr user.User
	err := d.sql.Get(&usr, "SELECT * FROM `users` WHERE `id`=? LIMIT 1", id)
	if errors.Is(err, sql.ErrNoRows) {
		d.Logger.Infof("Model not found by id=%s: %s", id, err)
		return nil, nil
	}
	if err != nil {
		d.Logger.Error("GetById err: ", err)
		return nil, err
	}
	d.Logger.Infof("User found by id=%s: %s", id, &usr)
	return &usr, err
}

func (d *Db) Create(u user.User) (*user.User, error) {

	res, err := d.sql.NamedExec("INSERT INTO `users` (`email`, `name`, `password_hash`, `created_at`) VALUES (:email,:name,:password_hash,:created_at)", u)
	if err != nil {
		d.Logger.Error("Create user error: ", err)
	}
	d.Logger.Info("Create user result: ", res)

	id, _ := res.LastInsertId()
	d.Logger.Infof("NEW ID: %d", id)
	strId := strconv.FormatInt(id, 10)
	d.Logger.Infof("NEW ID (str): %s", strId)
	return d.GetById(strId)
}

func NewStorage(sql *sqlx.DB, l echo.Logger) user.Storage {
	return &Db{sql: sql, Logger: l}
}
