/**
  NULL denied because it is GO :(
 */
CREATE TABLE IF NOT EXISTS users
(
    id            INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    email         VARCHAR(255) NOT NULL UNIQUE,
    name          VARCHAR(255) NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    created_at    DATETIME     NOT NULL DEFAULT '1970-01-01 00:00:00',
    logged_at     DATETIME     NOT NULL DEFAULT '1970-01-01 00:00:00'
);
CREATE TABLE IF NOT EXISTS posts
(
    id         INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id    INT UNSIGNED NOT NULL,
    title      VARCHAR(500) NOT NULL,
    body       TEXT         NOT NULL,
    slug       VARCHAR(255) NOT NULL,
    file       VARCHAR(255) NOT NULL,
    created_at DATETIME     NOT NULL DEFAULT '1970-01-01 00:00:00'

);

ALTER TABLE posts
    ADD FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE;