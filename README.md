# Echo REST API

Example of rest api service. Based on Go, Echo framework, mongodb, mysql.
Two databases implemented. Mysql and mongodb. Database can be switched in config.

## First of all:

### 1. Get token

Request:

```
curl -X 'POST' \
  'https://gorest.balumba.site/user/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'email=ivan%40yopmail.com&password=12345'
```

Response:

```
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc"
}
```

## And then:

### 2. All posts:

Request:

```
curl -X 'GET' \
  'https://gorest.balumba.site/api/posts?page=1' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc'
```

Response:

```
{
  "data": [
    {
      "id": "98",
      "user_id": "100",
      "title": "Iste et earum omnis ut libero.",
      "body": "Dolor id voluptatem et voluptas ut. Ipsa voluptatem praesentium non totam sint.",
      "slug": "iste-et-earum-omnis-ut-libero",
      "file": "",
      "created_at": "2023-04-07T10:18:11Z"
    },
    ...
  ]
  ...
}
```

### 3. Post by id:

Request:

```
curl -X 'GET' \
  'https://gorest.balumba.site/api/posts/98' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc'
```

Response:

```
{
  "data": {
    "id": "98",
    "user_id": "100",
    "title": "Iste et earum omnis ut libero.",
    "body": "Dolor id voluptatem et voluptas ut. Ipsa voluptatem praesentium non totam sint.",
    "slug": "iste-et-earum-omnis-ut-libero",
    "file": "",
    "created_at": "2023-04-07T10:18:11Z"
  }
}
```

### 4. Create post:

Request:

```
curl -X 'POST' \
  'https://gorest.balumba.site/api/posts' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc' \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "string",
  "body": "string"
}'
```

Response:

```
```

### 5. Update post:

Request:

```
curl -X 'PUT' \
  'https://gorest.balumba.site/api/posts/98' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc' \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "string",
  "body": "string"
}'
```

Response:

```
```

### 6. Delete post:

Request:

```
curl -X 'DELETE' \
  'https://gorest.balumba.site/api/posts/98' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMSIsIm5hbWUiOiJpdmFuQHlvcG1haWwuY29tIiwiaXNfYWRtaW4iOmZhbHNlLCJleHAiOjE2ODM3ODU5ODB9.LsTbIiRmRWn0r6FRnO5LzJ6fSX18g8rqDPH5ZbUE9Xc'
```

Response:

```
```

# How to use migrations

## Create

```
./migrate create -ext sql -dir migrations -seq create_users_table
```

## Up

```
./migrate -database "mysql://root:password@tcp(localhost:3306)/rest_api" -path ./migrations up
```

## Down

```
./migrate -database "mysql://root:password@tcp(localhost:3306)/rest_api" -path ./migrations down
```

## How to launch

1. ```cd /var/www/html/echo_rest```
2. ```docker compose down```
3. ```git pull origin main```
4. ```docker compose up --build -d```

After that two addresses become available:

1. ```localhost:8089```
2. ```localhost:1324```

# Database dump

## Mongo

1. ```mongodump --db rest_api```
2. ```mongorestore --host localhost -d rest_api --port 27017 --username=root --password=password --authenticationDatabase=admin --authenticationMechanism=SCRAM-SHA-256 /mongo_dump```